# **JourneyMap for Minecraft** Language Files

Language localization resources for [JourneyMap][2] ([http://journeymap.info][2])

## Current Translation Needs for JourneyMap 5.1.1

**As of November 7, 2015:**

* Translation for **jm.common.ui_theme_applied** in all *.lang* files (except en_US, ja_JP)
* Updated translation for all *message/splash-*.json* files (except en_US, ja_JP)
* Any new languages
## How to translate JourneyMap

Please see this wiki page for the full details:
http://journeymap.techbrew.net/help/wiki/Translate_JourneyMap_For_Your_Language

## How to submit your translation
 
1. Fork this repository with a Git tool, Source Tree, etc.
2. Update/add files as needed.
3. Changes to en_US.* files are generally not accepted unless they address typos. 
4. [Open a new Pull Request](https://bitbucket.org/TeamJM/journeymap-lang/pull-requests/new) (PR) with a title of *Translation for (language name)*.
5. In the description of the PR, indicate which version of Minecraft and JourneyMap you used 
6. If you see any problems with display of the translated messages, please make a note of it in the PR. For example, there may not be enough space provided to display a certain phrase. In that case, please note the message key and describe the problem you see.

[2]: http://journeymap.info